/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collection;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;




public class Collectiontest {

    
    public static void main(String []a){
       Map<String, Double> stu = new HashMap<>();
       stu.put("Ismet", 8.6);
       stu.put("Turxan", 6.7);
       stu.put("Murad", 5.6);
       stu.put("Nurlan", 3.7);
       stu.put("Ismail", 5.8);
       stu.put("Mura", 7.8);
       stu.put("Azer", 6.8);
       
       
       Map<String, Double> sortedStu = new LinkedHashMap<>();
       
       stu.entrySet().stream().sorted(Map.Entry.<String, Double>comparingByValue()).forEachOrdered(x -> sortedStu.put(x.getKey(), x.getValue()));
        System.out.println("Collection sorted");       
       System.out.println(sortedStu);
               
               Func func = (Map map) -> {
                   Set set =  map.entrySet();
                   Iterator iter = set.iterator();
                   double sum =0;
                   while(iter.hasNext()){
                       Map.Entry mapen = (Map.Entry ) iter.next();
                       sum =sum +(double)mapen.getValue();
                   }
                   return sum/map.size();
       };
               System.out.println("Avarage value of students gp");
        System.out.println(func.average(stu));
    }

    
}

