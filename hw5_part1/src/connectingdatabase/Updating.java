/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connectingdatabase;

import java.sql.*;


public class Updating {

    DBcreating db = new DBcreating();
    String newName;

    public Updating(String name) {
        setName(name);
    }

    public void setName(String name) {
        newName = name;
    }

    public String getNewName() {
        return newName;
    }

    //   String sql = "update `studentdata`.`students` SET first_name='" + getNewName() + "' WHERE idstudents='1'";
    public void update() {
        db.connect();

        try {
            Statement st = db.con.createStatement();
            st.execute("update `studentdata`.`students` SET first_name='" + getNewName() + "' WHERE idstudents='1'");
            db.con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
