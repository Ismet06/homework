/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lap3_shapes;

/**
 *
 * @author Ismer
 */
public class Lap3_shapes {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Rectangle rect = new Rectangle();
        Rectangle rectangle = new Rectangle(2, 2, 4, 6);
        System.out.println(rect.toString());
        System.out.println(rectangle.toString());
        rect.printShape();
        rectangle.printShape();

        Circle circle = new Circle();
        Circle shape = new Circle(4, 5, 3);
        System.out.println(circle.toString());
        System.out.println(shape.toString());
        circle.printShape();
        shape.printShape();

    }

}
