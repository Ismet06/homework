/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package excelJava;

import java.io.*;
import jxl.*;
import jxl.read.biff.BiffException;

/**
 *
 * @author Ismer
 */
public class Read {
    public static void main(String [] arg) throws IOException, BiffException{
       File f= new File("C:\\Users\\Ismer\\Documents\\myRepo\\homework\\lab6\\test\\javaexcel.xls"); 
        Workbook wb = Workbook.getWorkbook(f);
        Sheet sh = wb.getSheet(0);
        int row=sh.getRows();
        int col=sh.getColumns();
        for(int i=0; i<row; i++){
            for(int j=0; j<col; j++){
                Cell c = sh.getCell(j, i);
                System.out.print(" "+c.getContents());
            }
            System.out.println("");
        }
    }
}
