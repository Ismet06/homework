/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connectingdatabase;

import java.sql.*;

public class DBcreating {

    Connection con;
    String url = "jdbc:mysql://localhost:3306/";
    Statement st;

    public void connect() {
        try {
            con = DriverManager.getConnection(url, "root", "ismet.22");  //

        } catch (SQLException e) {
            System.out.println("Connection to database failed");
            //e.printStackTrace();
        }
    }

    public void createTable() {

        try {
            st = con.createStatement();
            st.execute("CREATE SCHEMA `studentdata`;");
            st.execute("CREATE TABLE `studentdata`.`students` (\n"
                    + "  `idstudents` INT NOT NULL,\n"
                    + "  `first_name` VARCHAR(45) NULL,\n"
                    + "  `last_name` VARCHAR(45) NULL,\n"
                    + "  `group_name` VARCHAR(45) NULL,\n"
                    + "  PRIMARY KEY (`idstudents`))");
            con.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
