/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connectingdatabase;

import java.sql.*;


public class ReadingData {

    DBcreating db = new DBcreating();

    public void read() {

        try {
            db.connect();
            Statement st = db.con.createStatement();

            ResultSet rs = st.executeQuery("SELECT * FROM `studentdata`.`students`");
            System.out.println("Studnet's name: ");
            while (rs.next()) {
                System.out.println(rs.getString(2) + " " + rs.getString(3));
            }
            db.con.close();
        } catch (SQLException e) {
            // e.printStackTrace();
            System.out.println("Data reading failed");
        }
    }

}
