


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;


public class AddRow {
    private static Connection con;
    private static PreparedStatement insertStudent;
    private static Statement statement;
    public static void main(String[] ar){
        try {
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/database","root", "ismet.22");
            insertStudent = con.prepareStatement("INSERT INTO new_table(`new_tablecol`, `new_tablecol1`) VALUES(?, ?)");
            statement = con.createStatement();
        } catch (SQLException ex) {
            return;
        }
        
        
        Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                insert("sdada", "sdassa");
                try {
                    ResultSet result = statement.executeQuery("SELECT * FROM new_table GROUP_BY(idnew_table/10)");
                    while ( result.next() )
                    {
                        System.out.println(result.getString(1) + " " + result.getString(2));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(AddRow.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }, 0, 2000);
      
    }
    
    private static void insert(String first, String last) {

        try {
            
            
            insertStudent.setString(1, first);
            insertStudent.setString(2, last);
            
            
            int i = insertStudent.executeUpdate();
            if (i != 0) {
                System.out.println("Student's information inserted");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
