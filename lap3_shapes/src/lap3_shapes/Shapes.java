/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lap3_shapes;

/**
 *
 * @author Ismer
 */
public abstract class Shapes {

    private double xPos;
    private double yPos;

    public Shapes() {
        this.xPos = this.yPos = 0;
    }

    public Shapes(double xPos, double yPos) {
        this.xPos = xPos;
        this.yPos = yPos;
    }

    public void setPos(double x, double y) {
                xPos = x;
                yPos = y;
    }

    public double getXPos() {
        return xPos;
    }

    public double getYPos() {
        return yPos;
    }

    @Override
    public String toString() {
        return "Shape(xPos, yPos";
    }

    public abstract double getArea();

    public abstract void printShape();
}
