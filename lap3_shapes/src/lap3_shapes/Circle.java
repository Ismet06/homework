/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lap3_shapes;

/**
 *
 * @author Ismer
 */
public class Circle extends Shapes {

    private double radius;

    Circle() {
        radius = 1;
    }

    Circle(double r) {
        radius = r;
    }

    Circle(double x, double y, double r) {
        super(x, y);
        radius = r;
    }

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public String toString() {
        return "Circle(xPos, yPos), radius";
    }

    @Override
    public void printShape() {
    }
}
