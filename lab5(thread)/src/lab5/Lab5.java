/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;



/**
 *
 * @author Ismer
 */
public class Lab5 extends Thread {

    public static void main(String[] args) {
        //using thread.sleep() waiting 5 second
        ThreadSleep t = new ThreadSleep();
        t.start();
        //using synchronized method 
        ThreadSynchro ts =new ThreadSynchro();
        Thread th =new Thread(){
            public void run(){
                ts.syn();
            }
        };
        Thread th1 =new Thread(){
            public void run(){
                ts.syn();
            }
        };
        th.start();
        th1.start();
        
                
        //using thread.yield() to 
       ThreadYield ty = new ThreadYield();
        Yield2 y = new Yield2();
        ty.start();
        y.start();
        // using join() method which is waiting completing thread's work
        ThreadJoin tj = new ThreadJoin();
        ThreadJoin tj1 = new ThreadJoin();
        ThreadJoin tj2 = new ThreadJoin();
        tj.start();
        
        try {
            tj.join();
        } catch (InterruptedException ex) {
            
        }
        tj1.start();
        tj2.start();
                
    }
}
