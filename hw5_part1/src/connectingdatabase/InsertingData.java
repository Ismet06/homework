/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connectingdatabase;

import java.sql.*;

public class InsertingData {

    ;
    PreparedStatement insertStudent;
    DBcreating db = new DBcreating();
    public void inserting(int id, String first, String last, String group) {

        try {
            db.connect();
            insertStudent = db.con.prepareStatement(" INSERT INTO `studentdata`.`students`(idstudents, "
                    + "first_name, last_name, group_name)"
                    + "VALUES(?, ?, ?, ?)");
            insertStudent.setInt(1, id);
            insertStudent.setString(2, first);
            insertStudent.setString(3, last);
            insertStudent.setString(4, group);
            
            int i = insertStudent.executeUpdate();
            if (i != 0) {
                System.out.println("Student's information inserted");
            }
            db.con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
