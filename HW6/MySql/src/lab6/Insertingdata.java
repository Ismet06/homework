/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab6;

import java.sql.*;

/**
 *
 * @author Ismer
 */
public class Insertingdata {

    Connecting c = new Connecting();
    PreparedStatement insertData;
    Statement st;

    public void insert( String name) {

        c.connect();
        try {
            int id=0;
            st = c.con.createStatement();
            ResultSet rs = st.executeQuery("SELECT max(iduser) FROM `users`");
            while(rs.next()){
            id = rs.getInt(1)+1;
            }
            insertData = c.con.prepareStatement("INSERT into `sql8168592`.`users`(iduser, user_name)"
                    + "VALUES(?, ?)");
            insertData.setInt(1, id);
            insertData.setString(2, name);
            insertData.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

    }
}
