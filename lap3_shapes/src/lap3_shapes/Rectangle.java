/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lap3_shapes;

/**
 *
 * @author Ismer
 */
public class Rectangle {

    private double xPos;
    private double yPos;
    private double width;
    private double height;

    public Rectangle() {

        xPos = yPos = 0;
        width = height = 0;
    }

    public Rectangle(double width, double height) {
        this();
        this.width = width;
        this.height = height;

    }

    public Rectangle(double xPos, double yPos, double width, double height) {
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = width;
        this.height = height;
    }

    public void setPos(double x, double y) {

    }

    public double getXPos() {
        return xPos;
    }

    public double getYPos() {
        return yPos;
    }

    double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    double getArea() {
        return width * height;
    }

    String tostring() {
        return "Rectangle(xPos, yPos)- (xPos*width + yPos*height)";
    }

    public void printShape() {
        for (int i = (int) width; i < (int) (height); i++) {
            for (int j = (int) width; j < (int) (height); j++) {
                System.out.print("*");
            }
            System.out.println("\n");
        }
    }
}
